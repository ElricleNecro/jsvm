#[macro_use] extern crate slog;
extern crate sloggers;

extern crate regex;

use sloggers::Build;
use sloggers::terminal::{TerminalLoggerBuilder, Destination};
use sloggers::types::Severity;

use std::fmt;
use std::fmt::Display;
use std::fs::File;
use std::io;
use std::io::BufReader;
use std::io::prelude::*;
use std::error::Error;

use regex::Regex;

use slog::Drain;
use slog::Logger;

static TOKEN_REGEX: &str = r"\s*([^,\s:]+):?(?:\s*,\s*)*;?";
static NUMBER_REGEX: &str = r"((?:0[Xx])*[[:xdigit:]]+)";
static REG_REGEX: &str = r"\$([[:alnum:]_]+)";

#[derive(Debug)]
pub struct Token {
    name: String,
    args: Vec<String>,
}

impl Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Token(name: {}, args: {{{}}})", self.name, self.args.join(", "))
    }
}

#[derive(Debug)]
pub enum TError {
    FileOpen(io::Error),
    Re(regex::Error)
}

impl Display for TError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &TError::FileOpen(ref i) => write!(f, "TError({})", i.description()),
            &TError::Re(ref r) => write!(f, "TError({})", r)
        }
    }
}

impl Error for TError {
    fn description(&self) -> &str {
        match self {
            &TError::FileOpen(ref i) => i.description(),
            &TError::Re(ref r) => r.description()
        }
    }

    fn cause(&self) -> Option<&Error> {
        match self {
            &TError::FileOpen(ref i) => i.cause(),
            &TError::Re(ref r) => r.cause()
        }
    }
}

pub fn parse(log: &Logger, fname: &str) -> Result<Vec<Token>, TError> {
        let file = match File::open(fname) {
            Ok(v) => v,
            Err(e) => return Err( TError::FileOpen(e) )
        };
        let mut buf_reader = BufReader::new(file);
        let mut buffer = String::new();
        let mut out = Vec::new();
        let re = match Regex::new(TOKEN_REGEX) {
            Ok(v) => v,
            Err(e) => return Err( TError::Re(e) )
        };

        while buf_reader.read_line(&mut buffer).unwrap() > 0 {
            let line = buffer.replace("\r", "").replace("\n", "");
            debug!(log, "String: '{}'", line);
            let mut re_iter = re.captures_iter(&line)
                .map(|x| x.get(1))
                .filter(|x| x.is_some())
                .map(|x| x.unwrap())
                ;
            let tok = Token {
                name: re_iter.nth(0).unwrap().as_str().to_string(),
                args: re_iter.map(|x| x.as_str().to_string()).collect()
            };
            debug!(log, "Token: '{}'", tok);
            out.push(
                tok
            );

            buffer.clear();
        }

        Ok(out)
}

fn main() {
    let mut builder = TerminalLoggerBuilder::new();
    builder.level(Severity::Debug);
    builder.destination(Destination::Stderr);

    let logger = builder.build().unwrap();

    let tokens = match parse(&logger, "test/one.asm") {
        Err(err) => panic!("Got an Error: {}", err),
        Ok(v) => v
    };

    info!(logger, "{:?}", tokens);
    for token in tokens {
        info!(logger, "{}", token);
    }
}
